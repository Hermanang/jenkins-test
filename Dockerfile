FROM httpd:2.4

ENV workspace /var/www/html
ENV mount_ /home/docker/html

COPY ${mount_} /usr/local/apache2/htdocs/
# Install PHP

RUN apt-get update
RUN apt install php libapache2-mod-php -y

# Install COMPOSER

RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
RUN php -r "if (hash_file('sha384', 'composer-setup.php') === 'a5c698ffe4b8e849a443b120cd5ba38043260d5c4023dbf93e1558871f1f07f58274fc6f4c93bcfd858c6bd0775cd8d1') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
RUN php composer-setup.php
RUN php -r "unlink('composer-setup.php');"
RUN mv composer.phar /usr/local/bin/composer

WORKDIR ${workspace}

EXPOSE 8099


